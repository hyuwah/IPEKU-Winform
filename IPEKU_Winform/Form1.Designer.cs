﻿namespace IPEKU_Winform
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            BunifuAnimatorNS.Animation animation2 = new BunifuAnimatorNS.Animation();
            BunifuAnimatorNS.Animation animation1 = new BunifuAnimatorNS.Animation();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.bDragControl = new Bunifu.Framework.UI.BunifuDragControl(this.components);
            this.titleBar = new System.Windows.Forms.Panel();
            this.btnMinimize = new Bunifu.Framework.UI.BunifuImageButton();
            this.btnMaximize = new Bunifu.Framework.UI.BunifuImageButton();
            this.btnClose = new Bunifu.Framework.UI.BunifuImageButton();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.bunifuCustomLabel1 = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.animSideMenu = new BunifuAnimatorNS.BunifuTransition(this.components);
            this.panel1 = new System.Windows.Forms.Panel();
            this.panelMain = new System.Windows.Forms.Panel();
            this.materialTabControl1 = new MaterialSkin.Controls.MaterialTabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPanelHome = new System.Windows.Forms.Panel();
            this.panelHome = new System.Windows.Forms.TableLayoutPanel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.gridControl1 = new DevExpress.XtraGrid.GridControl();
            this.gridView1 = new DevExpress.XtraGrid.Views.Grid.GridView();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.tsLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.tsTotalData = new System.Windows.Forms.ToolStripStatusLabel();
            this.tsLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.tsSelectedRow = new System.Windows.Forms.ToolStripStatusLabel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnpHomeDeleteAll = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btnpHomeAdd = new Bunifu.Framework.UI.BunifuFlatButton();
            this.lblIP = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.lblNilaiMutu = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.lblSKSTotal = new Bunifu.Framework.UI.BunifuCustomLabel();
            this.btnpHomeDelete = new Bunifu.Framework.UI.BunifuFlatButton();
            this.tbdHurufMutu = new Bunifu.Framework.UI.BunifuDropdown();
            this.tbSks = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.tbMatkul = new Bunifu.Framework.UI.BunifuMaterialTextbox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.panelSideMenu = new System.Windows.Forms.Panel();
            this.btnPanelAbout = new Bunifu.Framework.UI.BunifuFlatButton();
            this.bunifuFlatButton3 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.bunifuFlatButton2 = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btnPanelHome = new Bunifu.Framework.UI.BunifuFlatButton();
            this.btnSideMenu = new Bunifu.Framework.UI.BunifuImageButton();
            this.picLogo = new System.Windows.Forms.PictureBox();
            this.animLogo = new BunifuAnimatorNS.BunifuTransition(this.components);
            this.bElipseForm = new Bunifu.Framework.UI.BunifuElipse(this.components);
            this.bElipsePanel = new Bunifu.Framework.UI.BunifuElipse(this.components);
            this.titleBar.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnMinimize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnMaximize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnClose)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.panel1.SuspendLayout();
            this.panelMain.SuspendLayout();
            this.materialTabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPanelHome.SuspendLayout();
            this.panelHome.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).BeginInit();
            this.statusStrip1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.panelSideMenu.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnSideMenu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picLogo)).BeginInit();
            this.SuspendLayout();
            // 
            // bDragControl
            // 
            this.bDragControl.Fixed = true;
            this.bDragControl.Horizontal = true;
            this.bDragControl.TargetControl = this.titleBar;
            this.bDragControl.Vertical = true;
            // 
            // titleBar
            // 
            this.titleBar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(204)))));
            this.titleBar.Controls.Add(this.btnMinimize);
            this.titleBar.Controls.Add(this.btnMaximize);
            this.titleBar.Controls.Add(this.btnClose);
            this.titleBar.Controls.Add(this.pictureBox2);
            this.titleBar.Controls.Add(this.bunifuCustomLabel1);
            this.animLogo.SetDecoration(this.titleBar, BunifuAnimatorNS.DecorationType.None);
            this.animSideMenu.SetDecoration(this.titleBar, BunifuAnimatorNS.DecorationType.None);
            this.titleBar.Dock = System.Windows.Forms.DockStyle.Top;
            this.titleBar.Location = new System.Drawing.Point(0, 0);
            this.titleBar.Name = "titleBar";
            this.titleBar.Size = new System.Drawing.Size(801, 35);
            this.titleBar.TabIndex = 4;
            // 
            // btnMinimize
            // 
            this.btnMinimize.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnMinimize.BackColor = System.Drawing.Color.Transparent;
            this.animLogo.SetDecoration(this.btnMinimize, BunifuAnimatorNS.DecorationType.None);
            this.animSideMenu.SetDecoration(this.btnMinimize, BunifuAnimatorNS.DecorationType.None);
            this.btnMinimize.Image = ((System.Drawing.Image)(resources.GetObject("btnMinimize.Image")));
            this.btnMinimize.ImageActive = null;
            this.btnMinimize.Location = new System.Drawing.Point(726, 9);
            this.btnMinimize.Name = "btnMinimize";
            this.btnMinimize.Size = new System.Drawing.Size(18, 18);
            this.btnMinimize.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btnMinimize.TabIndex = 7;
            this.btnMinimize.TabStop = false;
            this.btnMinimize.Zoom = 10;
            this.btnMinimize.Click += new System.EventHandler(this.btnMinimize_Click);
            // 
            // btnMaximize
            // 
            this.btnMaximize.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnMaximize.BackColor = System.Drawing.Color.Transparent;
            this.animLogo.SetDecoration(this.btnMaximize, BunifuAnimatorNS.DecorationType.None);
            this.animSideMenu.SetDecoration(this.btnMaximize, BunifuAnimatorNS.DecorationType.None);
            this.btnMaximize.Image = ((System.Drawing.Image)(resources.GetObject("btnMaximize.Image")));
            this.btnMaximize.ImageActive = null;
            this.btnMaximize.Location = new System.Drawing.Point(750, 9);
            this.btnMaximize.Name = "btnMaximize";
            this.btnMaximize.Size = new System.Drawing.Size(18, 18);
            this.btnMaximize.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btnMaximize.TabIndex = 6;
            this.btnMaximize.TabStop = false;
            this.btnMaximize.Zoom = 10;
            this.btnMaximize.Click += new System.EventHandler(this.btnMaximize_Click);
            // 
            // btnClose
            // 
            this.btnClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClose.BackColor = System.Drawing.Color.Transparent;
            this.animLogo.SetDecoration(this.btnClose, BunifuAnimatorNS.DecorationType.None);
            this.animSideMenu.SetDecoration(this.btnClose, BunifuAnimatorNS.DecorationType.None);
            this.btnClose.Image = ((System.Drawing.Image)(resources.GetObject("btnClose.Image")));
            this.btnClose.ImageActive = null;
            this.btnClose.Location = new System.Drawing.Point(774, 9);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(18, 18);
            this.btnClose.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btnClose.TabIndex = 5;
            this.btnClose.TabStop = false;
            this.btnClose.Zoom = 10;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // pictureBox2
            // 
            this.animSideMenu.SetDecoration(this.pictureBox2, BunifuAnimatorNS.DecorationType.None);
            this.animLogo.SetDecoration(this.pictureBox2, BunifuAnimatorNS.DecorationType.None);
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(10, 6);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(24, 24);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 3;
            this.pictureBox2.TabStop = false;
            // 
            // bunifuCustomLabel1
            // 
            this.bunifuCustomLabel1.AutoSize = true;
            this.animSideMenu.SetDecoration(this.bunifuCustomLabel1, BunifuAnimatorNS.DecorationType.None);
            this.animLogo.SetDecoration(this.bunifuCustomLabel1, BunifuAnimatorNS.DecorationType.None);
            this.bunifuCustomLabel1.Font = new System.Drawing.Font("Century Gothic", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bunifuCustomLabel1.ForeColor = System.Drawing.Color.White;
            this.bunifuCustomLabel1.Location = new System.Drawing.Point(36, 7);
            this.bunifuCustomLabel1.Name = "bunifuCustomLabel1";
            this.bunifuCustomLabel1.Size = new System.Drawing.Size(53, 21);
            this.bunifuCustomLabel1.TabIndex = 2;
            this.bunifuCustomLabel1.Text = "IPEKU";
            // 
            // animSideMenu
            // 
            this.animSideMenu.AnimationType = BunifuAnimatorNS.AnimationType.HorizSlide;
            this.animSideMenu.Cursor = null;
            animation2.AnimateOnlyDifferences = true;
            animation2.BlindCoeff = ((System.Drawing.PointF)(resources.GetObject("animation2.BlindCoeff")));
            animation2.LeafCoeff = 0F;
            animation2.MaxTime = 1F;
            animation2.MinTime = 0F;
            animation2.MosaicCoeff = ((System.Drawing.PointF)(resources.GetObject("animation2.MosaicCoeff")));
            animation2.MosaicShift = ((System.Drawing.PointF)(resources.GetObject("animation2.MosaicShift")));
            animation2.MosaicSize = 0;
            animation2.Padding = new System.Windows.Forms.Padding(0);
            animation2.RotateCoeff = 0F;
            animation2.RotateLimit = 0F;
            animation2.ScaleCoeff = ((System.Drawing.PointF)(resources.GetObject("animation2.ScaleCoeff")));
            animation2.SlideCoeff = ((System.Drawing.PointF)(resources.GetObject("animation2.SlideCoeff")));
            animation2.TimeCoeff = 0F;
            animation2.TransparencyCoeff = 0F;
            this.animSideMenu.DefaultAnimation = animation2;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(63)))), ((int)(((byte)(70)))));
            this.panel1.Controls.Add(this.panelMain);
            this.panel1.Controls.Add(this.panelSideMenu);
            this.panel1.Controls.Add(this.titleBar);
            this.animLogo.SetDecoration(this.panel1, BunifuAnimatorNS.DecorationType.None);
            this.animSideMenu.SetDecoration(this.panel1, BunifuAnimatorNS.DecorationType.None);
            this.panel1.Location = new System.Drawing.Point(2, 2);
            this.panel1.Margin = new System.Windows.Forms.Padding(0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(801, 495);
            this.panel1.TabIndex = 0;
            // 
            // panelMain
            // 
            this.panelMain.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(63)))), ((int)(((byte)(70)))));
            this.panelMain.Controls.Add(this.materialTabControl1);
            this.animLogo.SetDecoration(this.panelMain, BunifuAnimatorNS.DecorationType.None);
            this.animSideMenu.SetDecoration(this.panelMain, BunifuAnimatorNS.DecorationType.None);
            this.panelMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelMain.Location = new System.Drawing.Point(200, 35);
            this.panelMain.Name = "panelMain";
            this.panelMain.Size = new System.Drawing.Size(601, 460);
            this.panelMain.TabIndex = 5;
            // 
            // materialTabControl1
            // 
            this.materialTabControl1.Controls.Add(this.tabPage1);
            this.materialTabControl1.Controls.Add(this.tabPage2);
            this.animLogo.SetDecoration(this.materialTabControl1, BunifuAnimatorNS.DecorationType.None);
            this.animSideMenu.SetDecoration(this.materialTabControl1, BunifuAnimatorNS.DecorationType.None);
            this.materialTabControl1.Depth = 0;
            this.materialTabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.materialTabControl1.Location = new System.Drawing.Point(0, 0);
            this.materialTabControl1.Margin = new System.Windows.Forms.Padding(0);
            this.materialTabControl1.MouseState = MaterialSkin.MouseState.HOVER;
            this.materialTabControl1.Name = "materialTabControl1";
            this.materialTabControl1.SelectedIndex = 0;
            this.materialTabControl1.Size = new System.Drawing.Size(601, 460);
            this.materialTabControl1.TabIndex = 2;
            // 
            // tabPage1
            // 
            this.tabPage1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(63)))), ((int)(((byte)(70)))));
            this.tabPage1.Controls.Add(this.tabPanelHome);
            this.animLogo.SetDecoration(this.tabPage1, BunifuAnimatorNS.DecorationType.None);
            this.animSideMenu.SetDecoration(this.tabPage1, BunifuAnimatorNS.DecorationType.None);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Size = new System.Drawing.Size(593, 434);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "tabPage1";
            // 
            // tabPanelHome
            // 
            this.tabPanelHome.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(63)))), ((int)(((byte)(70)))));
            this.tabPanelHome.Controls.Add(this.panelHome);
            this.animLogo.SetDecoration(this.tabPanelHome, BunifuAnimatorNS.DecorationType.None);
            this.animSideMenu.SetDecoration(this.tabPanelHome, BunifuAnimatorNS.DecorationType.None);
            this.tabPanelHome.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabPanelHome.Location = new System.Drawing.Point(0, 0);
            this.tabPanelHome.Name = "tabPanelHome";
            this.tabPanelHome.Size = new System.Drawing.Size(593, 434);
            this.tabPanelHome.TabIndex = 0;
            // 
            // panelHome
            // 
            this.panelHome.ColumnCount = 2;
            this.panelHome.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 280F));
            this.panelHome.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.panelHome.Controls.Add(this.panel3, 0, 0);
            this.panelHome.Controls.Add(this.panel2, 0, 0);
            this.animSideMenu.SetDecoration(this.panelHome, BunifuAnimatorNS.DecorationType.None);
            this.animLogo.SetDecoration(this.panelHome, BunifuAnimatorNS.DecorationType.None);
            this.panelHome.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelHome.Location = new System.Drawing.Point(0, 0);
            this.panelHome.Name = "panelHome";
            this.panelHome.RowCount = 1;
            this.panelHome.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.panelHome.Size = new System.Drawing.Size(593, 434);
            this.panelHome.TabIndex = 1;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.gridControl1);
            this.panel3.Controls.Add(this.statusStrip1);
            this.animLogo.SetDecoration(this.panel3, BunifuAnimatorNS.DecorationType.None);
            this.animSideMenu.SetDecoration(this.panel3, BunifuAnimatorNS.DecorationType.None);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(283, 3);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(307, 428);
            this.panel3.TabIndex = 11;
            // 
            // gridControl1
            // 
            this.animLogo.SetDecoration(this.gridControl1, BunifuAnimatorNS.DecorationType.None);
            this.animSideMenu.SetDecoration(this.gridControl1, BunifuAnimatorNS.DecorationType.None);
            this.gridControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridControl1.Location = new System.Drawing.Point(0, 0);
            this.gridControl1.MainView = this.gridView1;
            this.gridControl1.Name = "gridControl1";
            this.gridControl1.Size = new System.Drawing.Size(307, 406);
            this.gridControl1.TabIndex = 1;
            this.gridControl1.ViewCollection.AddRange(new DevExpress.XtraGrid.Views.Base.BaseView[] {
            this.gridView1});
            // 
            // gridView1
            // 
            this.gridView1.GridControl = this.gridControl1;
            this.gridView1.Name = "gridView1";
            this.gridView1.OptionsBehavior.Editable = false;
            this.gridView1.RowCellClick += new DevExpress.XtraGrid.Views.Grid.RowCellClickEventHandler(this.gridView1_RowCellClick);
            // 
            // statusStrip1
            // 
            this.animLogo.SetDecoration(this.statusStrip1, BunifuAnimatorNS.DecorationType.None);
            this.animSideMenu.SetDecoration(this.statusStrip1, BunifuAnimatorNS.DecorationType.None);
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsLabel1,
            this.tsTotalData,
            this.tsLabel2,
            this.tsSelectedRow});
            this.statusStrip1.Location = new System.Drawing.Point(0, 406);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(307, 22);
            this.statusStrip1.TabIndex = 2;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // tsLabel1
            // 
            this.tsLabel1.BackColor = System.Drawing.Color.Transparent;
            this.tsLabel1.Name = "tsLabel1";
            this.tsLabel1.Size = new System.Drawing.Size(69, 17);
            this.tsLabel1.Text = "Total Data : ";
            // 
            // tsTotalData
            // 
            this.tsTotalData.BackColor = System.Drawing.Color.Transparent;
            this.tsTotalData.Name = "tsTotalData";
            this.tsTotalData.Size = new System.Drawing.Size(41, 17);
            this.tsTotalData.Text = "Empty";
            // 
            // tsLabel2
            // 
            this.tsLabel2.BackColor = System.Drawing.Color.Transparent;
            this.tsLabel2.Margin = new System.Windows.Forms.Padding(21, 3, 0, 2);
            this.tsLabel2.Name = "tsLabel2";
            this.tsLabel2.Size = new System.Drawing.Size(86, 17);
            this.tsLabel2.Text = "Selected Row : ";
            // 
            // tsSelectedRow
            // 
            this.tsSelectedRow.BackColor = System.Drawing.Color.Transparent;
            this.tsSelectedRow.Name = "tsSelectedRow";
            this.tsSelectedRow.Size = new System.Drawing.Size(36, 17);
            this.tsSelectedRow.Text = "None";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.btnpHomeDeleteAll);
            this.panel2.Controls.Add(this.btnpHomeAdd);
            this.panel2.Controls.Add(this.lblIP);
            this.panel2.Controls.Add(this.lblNilaiMutu);
            this.panel2.Controls.Add(this.lblSKSTotal);
            this.panel2.Controls.Add(this.btnpHomeDelete);
            this.panel2.Controls.Add(this.tbdHurufMutu);
            this.panel2.Controls.Add(this.tbSks);
            this.panel2.Controls.Add(this.tbMatkul);
            this.animLogo.SetDecoration(this.panel2, BunifuAnimatorNS.DecorationType.None);
            this.animSideMenu.SetDecoration(this.panel2, BunifuAnimatorNS.DecorationType.None);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(3, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(274, 428);
            this.panel2.TabIndex = 1;
            // 
            // btnpHomeDeleteAll
            // 
            this.btnpHomeDeleteAll.Activecolor = System.Drawing.Color.Maroon;
            this.btnpHomeDeleteAll.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(63)))), ((int)(((byte)(70)))));
            this.btnpHomeDeleteAll.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnpHomeDeleteAll.BorderRadius = 0;
            this.btnpHomeDeleteAll.ButtonText = "Delete All";
            this.btnpHomeDeleteAll.Cursor = System.Windows.Forms.Cursors.Hand;
            this.animLogo.SetDecoration(this.btnpHomeDeleteAll, BunifuAnimatorNS.DecorationType.None);
            this.animSideMenu.SetDecoration(this.btnpHomeDeleteAll, BunifuAnimatorNS.DecorationType.None);
            this.btnpHomeDeleteAll.DisabledColor = System.Drawing.Color.Gray;
            this.btnpHomeDeleteAll.Enabled = false;
            this.btnpHomeDeleteAll.Iconcolor = System.Drawing.Color.Transparent;
            this.btnpHomeDeleteAll.Iconimage = ((System.Drawing.Image)(resources.GetObject("btnpHomeDeleteAll.Iconimage")));
            this.btnpHomeDeleteAll.Iconimage_right = null;
            this.btnpHomeDeleteAll.Iconimage_right_Selected = null;
            this.btnpHomeDeleteAll.Iconimage_Selected = null;
            this.btnpHomeDeleteAll.IconMarginLeft = 0;
            this.btnpHomeDeleteAll.IconMarginRight = 0;
            this.btnpHomeDeleteAll.IconRightVisible = false;
            this.btnpHomeDeleteAll.IconRightZoom = 0D;
            this.btnpHomeDeleteAll.IconVisible = false;
            this.btnpHomeDeleteAll.IconZoom = 36D;
            this.btnpHomeDeleteAll.IsTab = false;
            this.btnpHomeDeleteAll.Location = new System.Drawing.Point(30, 374);
            this.btnpHomeDeleteAll.Name = "btnpHomeDeleteAll";
            this.btnpHomeDeleteAll.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(63)))), ((int)(((byte)(70)))));
            this.btnpHomeDeleteAll.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnpHomeDeleteAll.OnHoverTextColor = System.Drawing.Color.White;
            this.btnpHomeDeleteAll.selected = false;
            this.btnpHomeDeleteAll.Size = new System.Drawing.Size(195, 32);
            this.btnpHomeDeleteAll.TabIndex = 10;
            this.btnpHomeDeleteAll.Text = "Delete All";
            this.btnpHomeDeleteAll.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnpHomeDeleteAll.Textcolor = System.Drawing.Color.White;
            this.btnpHomeDeleteAll.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnpHomeDeleteAll.Click += new System.EventHandler(this.btnpHomeDeleteAll_Click);
            // 
            // btnpHomeAdd
            // 
            this.btnpHomeAdd.Activecolor = System.Drawing.Color.Green;
            this.btnpHomeAdd.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(63)))), ((int)(((byte)(70)))));
            this.btnpHomeAdd.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnpHomeAdd.BorderRadius = 0;
            this.btnpHomeAdd.ButtonText = "Add";
            this.btnpHomeAdd.Cursor = System.Windows.Forms.Cursors.Hand;
            this.animLogo.SetDecoration(this.btnpHomeAdd, BunifuAnimatorNS.DecorationType.None);
            this.animSideMenu.SetDecoration(this.btnpHomeAdd, BunifuAnimatorNS.DecorationType.None);
            this.btnpHomeAdd.DisabledColor = System.Drawing.Color.Gray;
            this.btnpHomeAdd.Iconcolor = System.Drawing.Color.Transparent;
            this.btnpHomeAdd.Iconimage = ((System.Drawing.Image)(resources.GetObject("btnpHomeAdd.Iconimage")));
            this.btnpHomeAdd.Iconimage_right = null;
            this.btnpHomeAdd.Iconimage_right_Selected = null;
            this.btnpHomeAdd.Iconimage_Selected = null;
            this.btnpHomeAdd.IconMarginLeft = 0;
            this.btnpHomeAdd.IconMarginRight = 0;
            this.btnpHomeAdd.IconRightVisible = false;
            this.btnpHomeAdd.IconRightZoom = 0D;
            this.btnpHomeAdd.IconVisible = false;
            this.btnpHomeAdd.IconZoom = 36D;
            this.btnpHomeAdd.IsTab = false;
            this.btnpHomeAdd.Location = new System.Drawing.Point(29, 293);
            this.btnpHomeAdd.Name = "btnpHomeAdd";
            this.btnpHomeAdd.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(63)))), ((int)(((byte)(70)))));
            this.btnpHomeAdd.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.btnpHomeAdd.OnHoverTextColor = System.Drawing.Color.White;
            this.btnpHomeAdd.selected = false;
            this.btnpHomeAdd.Size = new System.Drawing.Size(194, 32);
            this.btnpHomeAdd.TabIndex = 5;
            this.btnpHomeAdd.Text = "Add";
            this.btnpHomeAdd.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnpHomeAdd.Textcolor = System.Drawing.Color.White;
            this.btnpHomeAdd.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnpHomeAdd.Click += new System.EventHandler(this.btnpHomeAdd_Click);
            // 
            // lblIP
            // 
            this.lblIP.AutoSize = true;
            this.animSideMenu.SetDecoration(this.lblIP, BunifuAnimatorNS.DecorationType.None);
            this.animLogo.SetDecoration(this.lblIP, BunifuAnimatorNS.DecorationType.None);
            this.lblIP.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIP.ForeColor = System.Drawing.Color.White;
            this.lblIP.Location = new System.Drawing.Point(19, 21);
            this.lblIP.Name = "lblIP";
            this.lblIP.Size = new System.Drawing.Size(95, 55);
            this.lblIP.TabIndex = 7;
            this.lblIP.Text = "IP :";
            // 
            // lblNilaiMutu
            // 
            this.lblNilaiMutu.AutoSize = true;
            this.animSideMenu.SetDecoration(this.lblNilaiMutu, BunifuAnimatorNS.DecorationType.None);
            this.animLogo.SetDecoration(this.lblNilaiMutu, BunifuAnimatorNS.DecorationType.None);
            this.lblNilaiMutu.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNilaiMutu.ForeColor = System.Drawing.Color.White;
            this.lblNilaiMutu.Location = new System.Drawing.Point(24, 143);
            this.lblNilaiMutu.Name = "lblNilaiMutu";
            this.lblNilaiMutu.Size = new System.Drawing.Size(121, 26);
            this.lblNilaiMutu.TabIndex = 9;
            this.lblNilaiMutu.Text = "Nilai Mutu :";
            // 
            // lblSKSTotal
            // 
            this.lblSKSTotal.AutoSize = true;
            this.animSideMenu.SetDecoration(this.lblSKSTotal, BunifuAnimatorNS.DecorationType.None);
            this.animLogo.SetDecoration(this.lblSKSTotal, BunifuAnimatorNS.DecorationType.None);
            this.lblSKSTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSKSTotal.ForeColor = System.Drawing.Color.White;
            this.lblSKSTotal.Location = new System.Drawing.Point(24, 99);
            this.lblSKSTotal.Name = "lblSKSTotal";
            this.lblSKSTotal.Size = new System.Drawing.Size(122, 26);
            this.lblSKSTotal.TabIndex = 8;
            this.lblSKSTotal.Text = "SKS Total :";
            // 
            // btnpHomeDelete
            // 
            this.btnpHomeDelete.Activecolor = System.Drawing.Color.Maroon;
            this.btnpHomeDelete.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(63)))), ((int)(((byte)(70)))));
            this.btnpHomeDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnpHomeDelete.BorderRadius = 0;
            this.btnpHomeDelete.ButtonText = "Delete";
            this.btnpHomeDelete.Cursor = System.Windows.Forms.Cursors.Hand;
            this.animLogo.SetDecoration(this.btnpHomeDelete, BunifuAnimatorNS.DecorationType.None);
            this.animSideMenu.SetDecoration(this.btnpHomeDelete, BunifuAnimatorNS.DecorationType.None);
            this.btnpHomeDelete.DisabledColor = System.Drawing.Color.Gray;
            this.btnpHomeDelete.Iconcolor = System.Drawing.Color.Transparent;
            this.btnpHomeDelete.Iconimage = ((System.Drawing.Image)(resources.GetObject("btnpHomeDelete.Iconimage")));
            this.btnpHomeDelete.Iconimage_right = null;
            this.btnpHomeDelete.Iconimage_right_Selected = null;
            this.btnpHomeDelete.Iconimage_Selected = null;
            this.btnpHomeDelete.IconMarginLeft = 0;
            this.btnpHomeDelete.IconMarginRight = 0;
            this.btnpHomeDelete.IconRightVisible = false;
            this.btnpHomeDelete.IconRightZoom = 0D;
            this.btnpHomeDelete.IconVisible = false;
            this.btnpHomeDelete.IconZoom = 36D;
            this.btnpHomeDelete.IsTab = false;
            this.btnpHomeDelete.Location = new System.Drawing.Point(29, 331);
            this.btnpHomeDelete.Name = "btnpHomeDelete";
            this.btnpHomeDelete.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(63)))), ((int)(((byte)(70)))));
            this.btnpHomeDelete.OnHovercolor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.btnpHomeDelete.OnHoverTextColor = System.Drawing.Color.White;
            this.btnpHomeDelete.selected = false;
            this.btnpHomeDelete.Size = new System.Drawing.Size(195, 32);
            this.btnpHomeDelete.TabIndex = 6;
            this.btnpHomeDelete.Text = "Delete";
            this.btnpHomeDelete.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnpHomeDelete.Textcolor = System.Drawing.Color.White;
            this.btnpHomeDelete.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnpHomeDelete.Click += new System.EventHandler(this.btnpHomeDelete_Click);
            // 
            // tbdHurufMutu
            // 
            this.tbdHurufMutu.BackColor = System.Drawing.Color.Transparent;
            this.tbdHurufMutu.BorderRadius = 3;
            this.animSideMenu.SetDecoration(this.tbdHurufMutu, BunifuAnimatorNS.DecorationType.None);
            this.animLogo.SetDecoration(this.tbdHurufMutu, BunifuAnimatorNS.DecorationType.None);
            this.tbdHurufMutu.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbdHurufMutu.ForeColor = System.Drawing.Color.White;
            this.tbdHurufMutu.Items = new string[] {
        "A",
        "B",
        "C",
        "D",
        "E"};
            this.tbdHurufMutu.Location = new System.Drawing.Point(136, 247);
            this.tbdHurufMutu.Name = "tbdHurufMutu";
            this.tbdHurufMutu.NomalColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(63)))), ((int)(((byte)(70)))));
            this.tbdHurufMutu.onHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.tbdHurufMutu.selectedIndex = 0;
            this.tbdHurufMutu.Size = new System.Drawing.Size(88, 24);
            this.tbdHurufMutu.TabIndex = 4;
            // 
            // tbSks
            // 
            this.tbSks.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.animLogo.SetDecoration(this.tbSks, BunifuAnimatorNS.DecorationType.None);
            this.animSideMenu.SetDecoration(this.tbSks, BunifuAnimatorNS.DecorationType.None);
            this.tbSks.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbSks.ForeColor = System.Drawing.Color.White;
            this.tbSks.HintForeColor = System.Drawing.Color.White;
            this.tbSks.HintText = "SKS";
            this.tbSks.isPassword = false;
            this.tbSks.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(204)))));
            this.tbSks.LineIdleColor = System.Drawing.Color.DimGray;
            this.tbSks.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(204)))));
            this.tbSks.LineThickness = 2;
            this.tbSks.Location = new System.Drawing.Point(30, 247);
            this.tbSks.Margin = new System.Windows.Forms.Padding(4);
            this.tbSks.Name = "tbSks";
            this.tbSks.Size = new System.Drawing.Size(99, 24);
            this.tbSks.TabIndex = 1;
            this.tbSks.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // tbMatkul
            // 
            this.tbMatkul.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.animLogo.SetDecoration(this.tbMatkul, BunifuAnimatorNS.DecorationType.None);
            this.animSideMenu.SetDecoration(this.tbMatkul, BunifuAnimatorNS.DecorationType.None);
            this.tbMatkul.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tbMatkul.ForeColor = System.Drawing.Color.White;
            this.tbMatkul.HintForeColor = System.Drawing.Color.White;
            this.tbMatkul.HintText = "Mata Kuliah";
            this.tbMatkul.isPassword = false;
            this.tbMatkul.LineFocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(204)))));
            this.tbMatkul.LineIdleColor = System.Drawing.Color.DimGray;
            this.tbMatkul.LineMouseHoverColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(204)))));
            this.tbMatkul.LineThickness = 2;
            this.tbMatkul.Location = new System.Drawing.Point(29, 215);
            this.tbMatkul.Margin = new System.Windows.Forms.Padding(4);
            this.tbMatkul.Name = "tbMatkul";
            this.tbMatkul.Size = new System.Drawing.Size(194, 24);
            this.tbMatkul.TabIndex = 0;
            this.tbMatkul.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // tabPage2
            // 
            this.tabPage2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(63)))), ((int)(((byte)(63)))), ((int)(((byte)(70)))));
            this.tabPage2.Controls.Add(this.button2);
            this.tabPage2.Controls.Add(this.button1);
            this.animLogo.SetDecoration(this.tabPage2, BunifuAnimatorNS.DecorationType.None);
            this.animSideMenu.SetDecoration(this.tabPage2, BunifuAnimatorNS.DecorationType.None);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Size = new System.Drawing.Size(593, 434);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "tabPage2";
            // 
            // button2
            // 
            this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.animSideMenu.SetDecoration(this.button2, BunifuAnimatorNS.DecorationType.None);
            this.animLogo.SetDecoration(this.button2, BunifuAnimatorNS.DecorationType.None);
            this.button2.Location = new System.Drawing.Point(502, 389);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 1;
            this.button2.Text = "button2";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.animSideMenu.SetDecoration(this.button1, BunifuAnimatorNS.DecorationType.None);
            this.animLogo.SetDecoration(this.button1, BunifuAnimatorNS.DecorationType.None);
            this.button1.Location = new System.Drawing.Point(32, 19);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 24);
            this.button1.TabIndex = 0;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // panelSideMenu
            // 
            this.panelSideMenu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.panelSideMenu.Controls.Add(this.btnPanelAbout);
            this.panelSideMenu.Controls.Add(this.bunifuFlatButton3);
            this.panelSideMenu.Controls.Add(this.bunifuFlatButton2);
            this.panelSideMenu.Controls.Add(this.btnPanelHome);
            this.panelSideMenu.Controls.Add(this.btnSideMenu);
            this.panelSideMenu.Controls.Add(this.picLogo);
            this.animLogo.SetDecoration(this.panelSideMenu, BunifuAnimatorNS.DecorationType.None);
            this.animSideMenu.SetDecoration(this.panelSideMenu, BunifuAnimatorNS.DecorationType.None);
            this.panelSideMenu.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelSideMenu.Location = new System.Drawing.Point(0, 35);
            this.panelSideMenu.Name = "panelSideMenu";
            this.panelSideMenu.Size = new System.Drawing.Size(200, 460);
            this.panelSideMenu.TabIndex = 3;
            // 
            // btnPanelAbout
            // 
            this.btnPanelAbout.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(204)))));
            this.btnPanelAbout.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.btnPanelAbout.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPanelAbout.BorderRadius = 0;
            this.btnPanelAbout.ButtonText = "About";
            this.btnPanelAbout.Cursor = System.Windows.Forms.Cursors.Hand;
            this.animLogo.SetDecoration(this.btnPanelAbout, BunifuAnimatorNS.DecorationType.None);
            this.animSideMenu.SetDecoration(this.btnPanelAbout, BunifuAnimatorNS.DecorationType.None);
            this.btnPanelAbout.DisabledColor = System.Drawing.Color.Gray;
            this.btnPanelAbout.Iconcolor = System.Drawing.Color.Transparent;
            this.btnPanelAbout.Iconimage = ((System.Drawing.Image)(resources.GetObject("btnPanelAbout.Iconimage")));
            this.btnPanelAbout.Iconimage_right = null;
            this.btnPanelAbout.Iconimage_right_Selected = null;
            this.btnPanelAbout.Iconimage_Selected = null;
            this.btnPanelAbout.IconMarginLeft = 0;
            this.btnPanelAbout.IconMarginRight = 0;
            this.btnPanelAbout.IconRightVisible = true;
            this.btnPanelAbout.IconRightZoom = 0D;
            this.btnPanelAbout.IconVisible = true;
            this.btnPanelAbout.IconZoom = 32D;
            this.btnPanelAbout.IsTab = true;
            this.btnPanelAbout.Location = new System.Drawing.Point(0, 248);
            this.btnPanelAbout.Name = "btnPanelAbout";
            this.btnPanelAbout.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.btnPanelAbout.OnHovercolor = System.Drawing.Color.DodgerBlue;
            this.btnPanelAbout.OnHoverTextColor = System.Drawing.Color.White;
            this.btnPanelAbout.selected = false;
            this.btnPanelAbout.Size = new System.Drawing.Size(200, 48);
            this.btnPanelAbout.TabIndex = 7;
            this.btnPanelAbout.Text = "About";
            this.btnPanelAbout.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnPanelAbout.Textcolor = System.Drawing.Color.White;
            this.btnPanelAbout.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPanelAbout.Click += new System.EventHandler(this.btnPanelAbout_Click);
            // 
            // bunifuFlatButton3
            // 
            this.bunifuFlatButton3.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(204)))));
            this.bunifuFlatButton3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.bunifuFlatButton3.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuFlatButton3.BorderRadius = 0;
            this.bunifuFlatButton3.ButtonText = "Menu 3";
            this.bunifuFlatButton3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.animLogo.SetDecoration(this.bunifuFlatButton3, BunifuAnimatorNS.DecorationType.None);
            this.animSideMenu.SetDecoration(this.bunifuFlatButton3, BunifuAnimatorNS.DecorationType.None);
            this.bunifuFlatButton3.DisabledColor = System.Drawing.Color.Gray;
            this.bunifuFlatButton3.Iconcolor = System.Drawing.Color.Transparent;
            this.bunifuFlatButton3.Iconimage = ((System.Drawing.Image)(resources.GetObject("bunifuFlatButton3.Iconimage")));
            this.bunifuFlatButton3.Iconimage_right = null;
            this.bunifuFlatButton3.Iconimage_right_Selected = null;
            this.bunifuFlatButton3.Iconimage_Selected = null;
            this.bunifuFlatButton3.IconMarginLeft = 0;
            this.bunifuFlatButton3.IconMarginRight = 0;
            this.bunifuFlatButton3.IconRightVisible = true;
            this.bunifuFlatButton3.IconRightZoom = 0D;
            this.bunifuFlatButton3.IconVisible = true;
            this.bunifuFlatButton3.IconZoom = 32D;
            this.bunifuFlatButton3.IsTab = true;
            this.bunifuFlatButton3.Location = new System.Drawing.Point(0, 194);
            this.bunifuFlatButton3.Name = "bunifuFlatButton3";
            this.bunifuFlatButton3.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.bunifuFlatButton3.OnHovercolor = System.Drawing.Color.DodgerBlue;
            this.bunifuFlatButton3.OnHoverTextColor = System.Drawing.Color.White;
            this.bunifuFlatButton3.selected = false;
            this.bunifuFlatButton3.Size = new System.Drawing.Size(200, 48);
            this.bunifuFlatButton3.TabIndex = 6;
            this.bunifuFlatButton3.Text = "Menu 3";
            this.bunifuFlatButton3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.bunifuFlatButton3.Textcolor = System.Drawing.Color.White;
            this.bunifuFlatButton3.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            // 
            // bunifuFlatButton2
            // 
            this.bunifuFlatButton2.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(204)))));
            this.bunifuFlatButton2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.bunifuFlatButton2.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.bunifuFlatButton2.BorderRadius = 0;
            this.bunifuFlatButton2.ButtonText = "Menu 2";
            this.bunifuFlatButton2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.animLogo.SetDecoration(this.bunifuFlatButton2, BunifuAnimatorNS.DecorationType.None);
            this.animSideMenu.SetDecoration(this.bunifuFlatButton2, BunifuAnimatorNS.DecorationType.None);
            this.bunifuFlatButton2.DisabledColor = System.Drawing.Color.Gray;
            this.bunifuFlatButton2.Iconcolor = System.Drawing.Color.Transparent;
            this.bunifuFlatButton2.Iconimage = ((System.Drawing.Image)(resources.GetObject("bunifuFlatButton2.Iconimage")));
            this.bunifuFlatButton2.Iconimage_right = null;
            this.bunifuFlatButton2.Iconimage_right_Selected = null;
            this.bunifuFlatButton2.Iconimage_Selected = null;
            this.bunifuFlatButton2.IconMarginLeft = 0;
            this.bunifuFlatButton2.IconMarginRight = 0;
            this.bunifuFlatButton2.IconRightVisible = true;
            this.bunifuFlatButton2.IconRightZoom = 0D;
            this.bunifuFlatButton2.IconVisible = true;
            this.bunifuFlatButton2.IconZoom = 32D;
            this.bunifuFlatButton2.IsTab = true;
            this.bunifuFlatButton2.Location = new System.Drawing.Point(0, 140);
            this.bunifuFlatButton2.Name = "bunifuFlatButton2";
            this.bunifuFlatButton2.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.bunifuFlatButton2.OnHovercolor = System.Drawing.Color.DodgerBlue;
            this.bunifuFlatButton2.OnHoverTextColor = System.Drawing.Color.White;
            this.bunifuFlatButton2.selected = false;
            this.bunifuFlatButton2.Size = new System.Drawing.Size(200, 48);
            this.bunifuFlatButton2.TabIndex = 5;
            this.bunifuFlatButton2.Text = "Menu 2";
            this.bunifuFlatButton2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.bunifuFlatButton2.Textcolor = System.Drawing.Color.White;
            this.bunifuFlatButton2.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            // 
            // btnPanelHome
            // 
            this.btnPanelHome.Activecolor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(204)))));
            this.btnPanelHome.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.btnPanelHome.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.btnPanelHome.BorderRadius = 0;
            this.btnPanelHome.ButtonText = "Home";
            this.btnPanelHome.Cursor = System.Windows.Forms.Cursors.Hand;
            this.animLogo.SetDecoration(this.btnPanelHome, BunifuAnimatorNS.DecorationType.None);
            this.animSideMenu.SetDecoration(this.btnPanelHome, BunifuAnimatorNS.DecorationType.None);
            this.btnPanelHome.DisabledColor = System.Drawing.Color.Gray;
            this.btnPanelHome.Iconcolor = System.Drawing.Color.Transparent;
            this.btnPanelHome.Iconimage = ((System.Drawing.Image)(resources.GetObject("btnPanelHome.Iconimage")));
            this.btnPanelHome.Iconimage_right = null;
            this.btnPanelHome.Iconimage_right_Selected = null;
            this.btnPanelHome.Iconimage_Selected = null;
            this.btnPanelHome.IconMarginLeft = 0;
            this.btnPanelHome.IconMarginRight = 0;
            this.btnPanelHome.IconRightVisible = true;
            this.btnPanelHome.IconRightZoom = 0D;
            this.btnPanelHome.IconVisible = true;
            this.btnPanelHome.IconZoom = 32D;
            this.btnPanelHome.IsTab = true;
            this.btnPanelHome.Location = new System.Drawing.Point(0, 86);
            this.btnPanelHome.Name = "btnPanelHome";
            this.btnPanelHome.Normalcolor = System.Drawing.Color.FromArgb(((int)(((byte)(45)))), ((int)(((byte)(45)))), ((int)(((byte)(48)))));
            this.btnPanelHome.OnHovercolor = System.Drawing.Color.DodgerBlue;
            this.btnPanelHome.OnHoverTextColor = System.Drawing.Color.White;
            this.btnPanelHome.selected = false;
            this.btnPanelHome.Size = new System.Drawing.Size(200, 48);
            this.btnPanelHome.TabIndex = 4;
            this.btnPanelHome.Text = "Home";
            this.btnPanelHome.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.btnPanelHome.Textcolor = System.Drawing.Color.White;
            this.btnPanelHome.TextFont = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnPanelHome.Click += new System.EventHandler(this.btnPanelHome_Click);
            // 
            // btnSideMenu
            // 
            this.btnSideMenu.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSideMenu.BackColor = System.Drawing.Color.Transparent;
            this.btnSideMenu.Cursor = System.Windows.Forms.Cursors.Hand;
            this.animLogo.SetDecoration(this.btnSideMenu, BunifuAnimatorNS.DecorationType.None);
            this.animSideMenu.SetDecoration(this.btnSideMenu, BunifuAnimatorNS.DecorationType.None);
            this.btnSideMenu.Image = ((System.Drawing.Image)(resources.GetObject("btnSideMenu.Image")));
            this.btnSideMenu.ImageActive = null;
            this.btnSideMenu.Location = new System.Drawing.Point(164, 6);
            this.btnSideMenu.Name = "btnSideMenu";
            this.btnSideMenu.Size = new System.Drawing.Size(24, 24);
            this.btnSideMenu.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.btnSideMenu.TabIndex = 3;
            this.btnSideMenu.TabStop = false;
            this.btnSideMenu.Zoom = 10;
            this.btnSideMenu.Click += new System.EventHandler(this.btnSideMenu_Click);
            // 
            // picLogo
            // 
            this.animSideMenu.SetDecoration(this.picLogo, BunifuAnimatorNS.DecorationType.None);
            this.animLogo.SetDecoration(this.picLogo, BunifuAnimatorNS.DecorationType.None);
            this.picLogo.Image = ((System.Drawing.Image)(resources.GetObject("picLogo.Image")));
            this.picLogo.Location = new System.Drawing.Point(10, 20);
            this.picLogo.Name = "picLogo";
            this.picLogo.Size = new System.Drawing.Size(138, 51);
            this.picLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picLogo.TabIndex = 2;
            this.picLogo.TabStop = false;
            // 
            // animLogo
            // 
            this.animLogo.AnimationType = BunifuAnimatorNS.AnimationType.HorizSlide;
            this.animLogo.Cursor = null;
            animation1.AnimateOnlyDifferences = true;
            animation1.BlindCoeff = ((System.Drawing.PointF)(resources.GetObject("animation1.BlindCoeff")));
            animation1.LeafCoeff = 0F;
            animation1.MaxTime = 1F;
            animation1.MinTime = 0F;
            animation1.MosaicCoeff = ((System.Drawing.PointF)(resources.GetObject("animation1.MosaicCoeff")));
            animation1.MosaicShift = ((System.Drawing.PointF)(resources.GetObject("animation1.MosaicShift")));
            animation1.MosaicSize = 0;
            animation1.Padding = new System.Windows.Forms.Padding(0);
            animation1.RotateCoeff = 0F;
            animation1.RotateLimit = 0F;
            animation1.ScaleCoeff = ((System.Drawing.PointF)(resources.GetObject("animation1.ScaleCoeff")));
            animation1.SlideCoeff = ((System.Drawing.PointF)(resources.GetObject("animation1.SlideCoeff")));
            animation1.TimeCoeff = 0F;
            animation1.TransparencyCoeff = 0F;
            this.animLogo.DefaultAnimation = animation1;
            this.animLogo.MaxAnimationTime = 1000;
            // 
            // bElipseForm
            // 
            this.bElipseForm.ElipseRadius = 8;
            this.bElipseForm.TargetControl = this;
            // 
            // bElipsePanel
            // 
            this.bElipsePanel.ElipseRadius = 8;
            this.bElipsePanel.TargetControl = this.panel1;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(122)))), ((int)(((byte)(204)))));
            this.ClientSize = new System.Drawing.Size(806, 500);
            this.Controls.Add(this.panel1);
            this.animSideMenu.SetDecoration(this, BunifuAnimatorNS.DecorationType.None);
            this.animLogo.SetDecoration(this, BunifuAnimatorNS.DecorationType.None);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MinimumSize = new System.Drawing.Size(800, 500);
            this.Name = "Form1";
            this.Text = "Form1";
            this.SizeChanged += new System.EventHandler(this.Form1_SizeChanged);
            this.titleBar.ResumeLayout(false);
            this.titleBar.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.btnMinimize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnMaximize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.btnClose)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panelMain.ResumeLayout(false);
            this.materialTabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPanelHome.ResumeLayout(false);
            this.panelHome.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.gridView1)).EndInit();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            this.panelSideMenu.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.btnSideMenu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picLogo)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Bunifu.Framework.UI.BunifuDragControl bDragControl;
        private BunifuAnimatorNS.BunifuTransition animLogo;
        private BunifuAnimatorNS.BunifuTransition animSideMenu;
        private System.Windows.Forms.Panel panel1;
        private Bunifu.Framework.UI.BunifuElipse bElipseForm;
        private Bunifu.Framework.UI.BunifuElipse bElipsePanel;
        private System.Windows.Forms.Panel titleBar;
        private Bunifu.Framework.UI.BunifuImageButton btnClose;
        private System.Windows.Forms.PictureBox pictureBox2;
        private Bunifu.Framework.UI.BunifuCustomLabel bunifuCustomLabel1;
        private System.Windows.Forms.Panel panelMain;
        private System.Windows.Forms.Panel panelSideMenu;
        private Bunifu.Framework.UI.BunifuFlatButton btnPanelAbout;
        private Bunifu.Framework.UI.BunifuFlatButton bunifuFlatButton3;
        private Bunifu.Framework.UI.BunifuFlatButton bunifuFlatButton2;
        private Bunifu.Framework.UI.BunifuFlatButton btnPanelHome;
        private Bunifu.Framework.UI.BunifuImageButton btnSideMenu;
        private System.Windows.Forms.PictureBox picLogo;
        private Bunifu.Framework.UI.BunifuImageButton btnMinimize;
        private Bunifu.Framework.UI.BunifuImageButton btnMaximize;
        private MaterialSkin.Controls.MaterialTabControl materialTabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.Panel tabPanelHome;
        private System.Windows.Forms.TableLayoutPanel panelHome;
        private System.Windows.Forms.Panel panel3;
        private DevExpress.XtraGrid.GridControl gridControl1;
        private DevExpress.XtraGrid.Views.Grid.GridView gridView1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel tsLabel1;
        private System.Windows.Forms.ToolStripStatusLabel tsTotalData;
        private System.Windows.Forms.ToolStripStatusLabel tsLabel2;
        private System.Windows.Forms.ToolStripStatusLabel tsSelectedRow;
        private System.Windows.Forms.Panel panel2;
        private Bunifu.Framework.UI.BunifuCustomLabel lblIP;
        private Bunifu.Framework.UI.BunifuCustomLabel lblNilaiMutu;
        private Bunifu.Framework.UI.BunifuCustomLabel lblSKSTotal;
        private Bunifu.Framework.UI.BunifuFlatButton btnpHomeAdd;
        private Bunifu.Framework.UI.BunifuDropdown tbdHurufMutu;
        private Bunifu.Framework.UI.BunifuMaterialTextbox tbSks;
        private Bunifu.Framework.UI.BunifuMaterialTextbox tbMatkul;
        private System.Windows.Forms.TabPage tabPage2;
        private Bunifu.Framework.UI.BunifuFlatButton btnpHomeDelete;
        private Bunifu.Framework.UI.BunifuFlatButton btnpHomeDeleteAll;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button1;
    }
}

