﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;
using BunifuAnimatorNS;

namespace IPEKU_Winform
{
    public partial class Form1 : Form
    {

        #region Wajib Buat Borderless bisa resize

        //***********************************************************
        //This gives us the ability to resize the borderless from any borders instead of just the lower right corner
        protected override void WndProc(ref Message m)
        {
            const int wmNcHitTest = 0x84;
            const int htLeft = 10;
            const int htRight = 11;
            const int htTop = 12;
            const int htTopLeft = 13;
            const int htTopRight = 14;
            const int htBottom = 15;
            const int htBottomLeft = 16;
            const int htBottomRight = 17;

            if (m.Msg == wmNcHitTest)
            {
                int x = (int)(m.LParam.ToInt64() & 0xFFFF);
                int y = (int)((m.LParam.ToInt64() & 0xFFFF0000) >> 16);
                Point pt = PointToClient(new Point(x, y));
                Size clientSize = ClientSize;
                ///allow resize on the lower right corner
                if (pt.X >= clientSize.Width - 16 && pt.Y >= clientSize.Height - 16 && clientSize.Height >= 16)
                {
                    m.Result = (IntPtr)(IsMirrored ? htBottomLeft : htBottomRight);
                    return;
                }
                ///allow resize on the lower left corner
                if (pt.X <= 16 && pt.Y >= clientSize.Height - 16 && clientSize.Height >= 16)
                {
                    m.Result = (IntPtr)(IsMirrored ? htBottomRight : htBottomLeft);
                    return;
                }
                ///allow resize on the upper right corner
                if (pt.X <= 16 && pt.Y <= 16 && clientSize.Height >= 16)
                {
                    m.Result = (IntPtr)(IsMirrored ? htTopRight : htTopLeft);
                    return;
                }
                ///allow resize on the upper left corner
                if (pt.X >= clientSize.Width - 16 && pt.Y <= 16 && clientSize.Height >= 16)
                {
                    m.Result = (IntPtr)(IsMirrored ? htTopLeft : htTopRight);
                    return;
                }
                ///allow resize on the top border
                if (pt.Y <= 16 && clientSize.Height >= 16)
                {
                    m.Result = (IntPtr)(htTop);
                    return;
                }
                ///allow resize on the bottom border
                if (pt.Y >= clientSize.Height - 16 && clientSize.Height >= 16)
                {
                    m.Result = (IntPtr)(htBottom);
                    return;
                }
                ///allow resize on the left border
                if (pt.X <= 16 && clientSize.Height >= 16)
                {
                    m.Result = (IntPtr)(htLeft);
                    return;
                }
                ///allow resize on the right border
                if (pt.X >= clientSize.Width - 16 && clientSize.Height >= 16)
                {
                    m.Result = (IntPtr)(htRight);
                    return;
                }
            }
            base.WndProc(ref m);
        }

        //***********************************************************
        //***********************************************************
        //This gives us the ability to drag the borderless form to a new location
        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [DllImport("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);

        [DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();

        private void YOURCONTROL_MouseDown(object sender, MouseEventArgs e)
        {
            //ctrl-leftclick anywhere on the control to drag the form to a new location 
            if (e.Button == MouseButtons.Left && Control.ModifierKeys == Keys.Control)
            {
                ReleaseCapture();
                SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
            }
        }

        //***********************************************************
        //***********************************************************
        //This gives us the drop shadow behind the borderless form
        private const int CS_DROPSHADOW = 0x20000;

        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams cp = base.CreateParams;
                cp.ClassStyle |= CS_DROPSHADOW;
                return cp;
            }
        }


        //***********************************************************

        #endregion

        public Form1()
        {
            InitializeComponent();
            gridControl1.DataSource = DMList;
            btnPanelHome.selected = true;
            btnpHomeAdd.IconVisible = true;
            btnpHomeDelete.IconVisible = true;
            btnpHomeDeleteAll.IconVisible = true;

           
        }

        #region Winform Custom Control Button

        private void btnMaximize_Click(object sender, EventArgs e)
        {
            if (this.WindowState == FormWindowState.Maximized)
            {
                this.WindowState = FormWindowState.Normal;
            }
            else
            {
                this.WindowState = FormWindowState.Maximized;
            }
        }

        private void btnMinimize_Click(object sender, EventArgs e)
        {
            this.WindowState = System.Windows.Forms.FormWindowState.Minimized;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        #endregion


        private void btnSideMenu_Click(object sender, EventArgs e)
        {
            
            if (panelSideMenu.Width == 48)
            {
                
                panelSideMenu.Visible = false;
                panelSideMenu.Width = 200;
                animSideMenu.ShowSync(panelSideMenu);
                if (animSideMenu.IsCompleted)
                {
                    animLogo.Show(picLogo);
                }

            }
            else
            {
                
                animLogo.HideSync(picLogo);
                if (animLogo.IsCompleted)
                {
                    panelSideMenu.Visible = false;
                    panelSideMenu.Width = 48;
                    animSideMenu.Show(panelSideMenu);
                }



            }
        }

        private void btnPanelHome_Click(object sender, EventArgs e)
        {
            materialTabControl1.SelectedTab = tabPage1;
            
        }

        private void btnPanelAbout_Click(object sender, EventArgs e)
        {
            materialTabControl1.SelectedTab = tabPage2;
        }


        List<DataMatkul> DMList = new List<DataMatkul>();

        private void btnpHomeAdd_Click(object sender, EventArgs e)
        {
            try
            {
                DataMatkul dm = new DataMatkul();
                dm.MataKuliah = tbMatkul.Text;
                dm.SKS = Convert.ToInt32(tbSks.Text);
                dm.hurufMutu = Convert.ToChar(tbdHurufMutu.selectedValue);



                DMList.Add(dm);

                gridControl1.DataSource = DMList;
                gridControl1.RefreshDataSource();

                tsTotalData.Text = DMList.Count.ToString();

                calculate();

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
            }



        }

        private void btnpHomeDelete_Click(object sender, EventArgs e)
        {
            if (tsSelectedRow.Text != "None")
            {
                DMList.RemoveAt(Convert.ToInt32(tsSelectedRow.Text) - 1);
                gridControl1.RefreshDataSource();
                tsSelectedRow.Text = "None";
                tsTotalData.Text = DMList.Count.ToString();
                if (tsTotalData.Text == "0") tsTotalData.Text = "Empty";
                calculate();
            }
            else
            {
                Console.WriteLine("Pilih dulu");
            }

        }

        private void gridView1_RowCellClick(object sender, DevExpress.XtraGrid.Views.Grid.RowCellClickEventArgs e)
        {
            tsSelectedRow.Text = (e.RowHandle + 1).ToString();
        }


        private void calculate()
        {
            int sksTotal = 0;
            float nilaiMutuTotal = 0;
            float ip = 0;

            float nilaiMutu = 0;
            foreach (var dm in DMList)
            {
                switch (dm.hurufMutu)
                {
                    case 'A':
                        nilaiMutu = dm.SKS * 4;
                        break;
                    case 'B':
                        nilaiMutu = dm.SKS * 3;
                        break;
                    case 'C':
                        nilaiMutu = dm.SKS * 2;
                        break;
                    case 'D':
                        nilaiMutu = dm.SKS * 1;
                        break;
                    case 'E':
                        nilaiMutu = dm.SKS * 0;
                        break;
                }

                nilaiMutuTotal += nilaiMutu;
                sksTotal += dm.SKS;

                ip = nilaiMutuTotal / sksTotal;

                lblIP.Text = "IP : " + ip.ToString("##.##");
                lblSKSTotal.Text = "SKS Total : " + sksTotal;
                lblNilaiMutu.Text = "Nilai Mutu : " + nilaiMutuTotal;
            }
            if (DMList.Count > 0) btnpHomeDeleteAll.Enabled = true;
            else btnpHomeDeleteAll.Enabled = false;
        }

        private void btnpHomeDeleteAll_Click(object sender, EventArgs e)
        {
            if (DMList.Count > 0)
            {
                if (MessageBox.Show("Delete All?", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning,
                    MessageBoxDefaultButton.Button2) == DialogResult.Yes)
                {

                    DMList.Clear();
                    gridControl1.RefreshDataSource();

                    btnpHomeDeleteAll.Enabled = false;
                    tsTotalData.Text = "Empty";
                    tsSelectedRow.Text = "None";


                }
            }
            else
            {
                Console.WriteLine("No data to delete");
            }
        }

        private void Form1_SizeChanged(object sender, EventArgs e)
        {
            materialTabControl1.Refresh();
        }
    }
}
